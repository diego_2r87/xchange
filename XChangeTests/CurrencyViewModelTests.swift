//
//  CurrencyViewModelTests.swift
//  XChangeTests
//
//  Created by Diego Rincon on 11/6/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import XCTest

class CurrencyViewModelTests: XCTestCase {
    
    func testSuccessfulCurrencyViewModel() {
        let currencyData = ["flag": "USD",
                            "code": "USD",
                            "name": "United States Dollar",
                            "symbol": "$"]
        
        let currency = CurrencyViewModel(json: currencyData)
        
        XCTAssertNotNil(currency)
        XCTAssertNotNil(currency?.flag)
        XCTAssertEqual(currency?.code, "USD")
        XCTAssertEqual(currency?.name, "United States Dollar")
        XCTAssertEqual(currency?.symbol, "$")
    }
    
    func testMissingFlag() {
        let currencyData = ["code": "USD",
                            "name": "United States Dollar",
                            "symbol": "$"]
        
        let currency = CurrencyViewModel(json: currencyData)
        
        XCTAssertNil(currency)
    }
    
    func testMissingCode() {
        let currencyData = ["flag": "USD",
                            "name": "United States Dollar",
                            "symbol": "$"]
        
        let currency = CurrencyViewModel(json: currencyData)
        
        XCTAssertNil(currency)
    }
    
    func testMissingName() {
        let currencyData = ["flag": "USD",
                            "code": "USD",
                            "symbol": "$"]
        
        let currency = CurrencyViewModel(json: currencyData)
        
        XCTAssertNil(currency)
    }
    
    func testMissingSymbol() {
        let currencyData = ["flag": "USD",
                            "code": "USD",
                            "name": "United States Dollar"]
        
        let currency = CurrencyViewModel(json: currencyData)
        
        XCTAssertNil(currency)
    }
}
