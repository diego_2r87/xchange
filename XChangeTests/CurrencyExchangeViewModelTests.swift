//
//  CurrencyExchangeViewModelTests.swift
//  XChangeTests
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import XCTest
import RxSwift

class CurrencyExchangeViewModelTests: XCTestCase {
    
    let defaultTimeOut = TimeInterval(1.0)
    let disposeBag = DisposeBag()
    
    var viewModel: CurrencyExchangeViewModel?
    var expectation: XCTestExpectation?
    
    override func setUp() {
        super.setUp()
        
        viewModel = CurrencyExchangeViewModel()
        
        expectation = expectation(description: "runAsynchronousTest")
    }
    
    func testForceExchangeRateUpdates() {
        viewModel?.exchangeRatesRepository = FauxExchangeRatesRepository()
        viewModel?.forceExchangeRatesUpdate()
        
        viewModel?.exchangeRatesSubject.subscribe(onNext: { (exchangeRates) in
            guard let exchangeRates = exchangeRates else {
                return
            }
            
            let expectedExchangeRates = ["AUD": 1.2982,
                                         "BGN": 1.6795,
                                         "BRL": 3.2672,
                                         "CAD": 1.2847,
                                         "CHF":1.0002]
            
            XCTAssertEqual(exchangeRates.count, expectedExchangeRates.count)
            
            for (key, value) in expectedExchangeRates {
                XCTAssertEqual(exchangeRates[key], value)
            }
            
            self.expectation?.fulfill()
        }).disposed(by: disposeBag)
        
        waitForExpectations(timeout: defaultTimeOut, handler: nil)
    }
    
    func testForceExchangeRateUpdateError() {
        let repository = FauxExchangeRatesRepository()
        repository.requestError = .unableToMakeRequest
        
        viewModel?.exchangeRatesRepository = repository
        viewModel?.forceExchangeRatesUpdate()
        
        viewModel?.exchangeRatesSubject.subscribe(onError: { (error) in
            XCTAssertNotNil(error)
            
            self.expectation?.fulfill()
        }).disposed(by: disposeBag)
        
        waitForExpectations(timeout: defaultTimeOut, handler: nil)
    }
    
    func testExchangeUSD() {
        viewModel?.exchangeRatesRepository = FauxExchangeRatesRepository()
        viewModel?.forceExchangeRatesUpdate()
        
        viewModel?.exchangeRatesSubject.subscribe(onNext: { (exchangeRates) in
            guard let exchangeRates = exchangeRates else {
                return
            }
            
            for (key, value) in exchangeRates {
                let usd = Double(1000)
                let currencyValue = self.viewModel!.exchangeUSD(total: usd, to: key)
                let expectedCurrency = value * usd
                let delta = Double(0.01)
                
                XCTAssert(currencyValue > expectedCurrency - delta && currencyValue < expectedCurrency + delta)
            }
            
            self.expectation?.fulfill()
        }).disposed(by: disposeBag)
        
        waitForExpectations(timeout: defaultTimeOut, handler: nil)
    }
}
