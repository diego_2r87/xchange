//
//  AppConfigTests.swift
//  XChangeTests
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import XCTest

class AppConfigTests: XCTestCase {
    
    func testAppConfigA() {
        let appConfig = AppConfig()
        
        XCTAssertEqual(appConfig.scheme, "http")
        XCTAssertEqual(appConfig.host, "test.url.a.com")
        XCTAssertEqual(appConfig.rateExchangeEndpoint, "/test_endpoint_a")
    }
}
