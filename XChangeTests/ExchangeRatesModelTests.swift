//
//  ExchangeRatesModelTests.swift
//  XChangeTests
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import XCTest

class ExchangeRatesModelTests: XCTestCase {
    
    func testSuccessfulInitializer() {
        guard let exchangeRatesJSON = TestHelper.jsonDicFromJSONFile(named: "ExchangeRatesCorrect") else {
            XCTFail("Unable to load test file")
            return
        }
        
        let expectedExchangeRates = ["AUD": 1.2982,
                                     "BGN": 1.6795,
                                     "BRL": 3.2672,
                                     "CAD": 1.2847,
                                     "CHF": 1.0002]
        
        let exchangeRatesModel = ExchangeRatesModel(json: exchangeRatesJSON)
        
        XCTAssertNotNil(exchangeRatesModel)
        XCTAssertEqual(exchangeRatesModel?.baseCurrencyCode, "USD")
        XCTAssertEqual(exchangeRatesModel?.update, "2017-11-02")
        
        guard let exchangeRates = exchangeRatesModel?.exchangeRates else {
            XCTFail("Unable to load exchange rates")
            return
        }
        
        XCTAssertEqual(exchangeRates.count, expectedExchangeRates.count)
        
        for (key, value) in expectedExchangeRates {
            XCTAssertEqual(exchangeRates[key], value)
        }
    }
    
    func testInvalidJSON() {
        XCTAssertNil(TestHelper.jsonDicFromJSONFile(named: "ExchangeRatesInvalidJSON"))
    }
    
    func testNoBase() {
        guard let exchangeRatesJSON = TestHelper.jsonDicFromJSONFile(named: "ExchangeRatesNoBase") else {
            XCTFail("Unable to load test file")
            return
        }
        
        XCTAssertNil(ExchangeRatesModel(json: exchangeRatesJSON))
    }
    
    func testNoDate() {
        guard let exchangeRatesJSON = TestHelper.jsonDicFromJSONFile(named: "ExchangeRatesNoDate") else {
            XCTFail("Unable to load test file")
            return
        }
        
        XCTAssertNotNil(ExchangeRatesModel(json: exchangeRatesJSON))
    }
    
    func testNoRates() {
        guard let exchangeRatesJSON = TestHelper.jsonDicFromJSONFile(named: "ExchangeRatesNoRates") else {
            XCTFail("Unable to load test file")
            return
        }
        
        XCTAssertNil(ExchangeRatesModel(json: exchangeRatesJSON))
    }
    
    func testInvalidRates() {
        guard let exchangeRatesJSON = TestHelper.jsonDicFromJSONFile(named: "ExchangeRatesWithInvalidRates") else {
            XCTFail("Unable to load test file")
            return
        }
        
        guard let exchangeRates = ExchangeRatesModel(json: exchangeRatesJSON)?.exchangeRates else {
            XCTFail("Unable to load exchange rates")
            return
        }
        
        let expectedExchangeRates = ["AUD": 1.2982, "BRL": 3.2672]
        
        XCTAssertEqual(exchangeRates.count, expectedExchangeRates.count)
        
        for (key, value) in expectedExchangeRates {
            XCTAssertEqual(exchangeRates[key], value)
        }
    }
}
