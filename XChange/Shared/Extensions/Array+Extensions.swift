//
//  Array+Extensions.swift
//  XChange
//
//  Created by Diego Rincon on 11/6/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

extension Array {
    subscript (safe index: Int) -> Element? {
        return self.indices ~= index ? self[index] : nil
    }
}
