//
//  UIColor+Extensions.swift
//  XChange
//
//  Created by Diego Rincon on 10/31/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red255: CGFloat, green255: CGFloat, blue255: CGFloat, alpha: CGFloat = 1.0) {
        self.init(red: red255/255.0, green: green255/255.0, blue: blue255/255.0, alpha: alpha)
    }
}
