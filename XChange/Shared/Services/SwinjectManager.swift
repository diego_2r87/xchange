//
//  SwinjectManager.swift
//  XChange
//
//  Created by Diego Rincon on 10/31/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation
import Swinject

class SwinjectManager {
    
    let container: Container
    
    init() {
        container = Container()
        registerDependencies()
    }
    
    func registerDependencies() {
        registerViewControllers()
        registerViewModels()
        registerRepositories()
        registerServices()
    }
    
    // MARK: ViewControllers
    
    func registerViewControllers() {
        container.register(CurrencyConverterViewController.self) { r in
            let viewController = CurrencyConverterViewController()
            viewController.container = self.container
            
            return viewController
        }
        
        container.register(LoadingLaunchViewController.self) { r in
            let viewController = LoadingLaunchViewController()
            viewController.container = self.container
            viewController.currencyExchangeViewModel = r.resolve(CurrencyExchangeViewModel.self)
            
            return viewController
        }
    }
    
    func registerViewModels() {
        container.register(CurrencyExchangeViewModel.self) { r in
            let viewModel = CurrencyExchangeViewModel()
            viewModel.exchangeRatesRepository = r.resolve(ExchangeRatesRepository.self)
            viewModel.currencyDataRepository = r.resolve(CurrencyDataRepository.self)
            
            return viewModel
        }
    }
    
    func registerRepositories() {
        container.register(ExchangeRatesRepository.self) { r in
            let repository = ExchangeRatesRepository()
            repository.appConfig = r.resolve(AppConfig.self)
            
            return repository
        }
        
        container.register(CurrencyDataRepository.self) { r in
            return CurrencyDataRepository()
        }
    }
    
    func registerServices() {
        container.register(AppConfig.self) { r in
            return AppConfig()
        }
    }
}
