//
//  AppConfig.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

class AppConfig {
    
    enum Config {
        static let defaultEnvironment       = "prod"
        static let environmentKey           = "environment"
        
        static let fileExtension            = "json"
        static let configFileBase           = "config"
        
        static let schemeKey                = "scheme"
        static let hostKey                  = "host"
        static let rateExchangeEndpointKey  = "rate_exchange_endpoint"
    }
    
    let configVariables: [String: Any]?
    
    var scheme: String? {
        return configVariables?[Config.schemeKey] as? String
    }
    
    var host: String? {
        return configVariables?[Config.hostKey] as? String
    }
    
    var rateExchangeEndpoint: String? {
        return configVariables?[Config.rateExchangeEndpointKey] as? String
    }
    
    // MARK: Initializers
    
    init() {
        let environment = ProcessInfo.processInfo.environment[Config.environmentKey] ?? Config.defaultEnvironment
        let fileName = "\(Config.configFileBase).\(environment)"

        guard
            let fileURL = Bundle.main.url(forResource: fileName, withExtension: Config.fileExtension),
            let fileData = try? Data(contentsOf: fileURL),
            let jsonData = try? JSONSerialization.jsonObject(with: fileData, options: .allowFragments),
            let jsonDic = jsonData as? [String: Any] else {
                configVariables = nil
                return
        }

        configVariables = jsonDic
    }
}
