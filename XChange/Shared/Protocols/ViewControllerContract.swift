//
//  ViewControllerContract.swift
//  XChange
//
//  Created by Diego Rincon on 10/31/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation
import Swinject

protocol ViewControllerContract {
    var container: Container? { get }
}
