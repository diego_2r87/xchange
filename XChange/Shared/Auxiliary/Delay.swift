//
//  Delay.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

struct Delay {
    static func main(seconds: Double, execute: @escaping () -> Void) {
        let deadline = DispatchTime.now() + seconds
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: execute)
    }
}
