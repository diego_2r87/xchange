//
//  TestHelper.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

struct TestHelper {
    static func jsonDicFromJSONFile(named name: String) -> [String: Any]? {
        guard
            let fileURL = Bundle.main.url(forResource: name, withExtension: "json"),
            let fileData = try? Data(contentsOf: fileURL),
            let jsonData = try? JSONSerialization.jsonObject(with: fileData, options: .allowFragments),
            let jsonDic = jsonData as? [String: Any] else {
                return nil
        }
        
        return jsonDic
    }
}
