//
//  JSONParser.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

struct JSONParser {
    
    let json: [String: Any]
    
    init(json: [String: Any]) {
        self.json = json
    }
    
    func objectForKey<T>(_ key: String, classType: T.Type) -> T? {
        guard let object = json[key] as? T else {
            return nil
        }
        
        return object
    }
    
    func objectForKey<T>(_ key: String, defaultObject: T) -> T {
        guard let object = objectForKey(key, classType: T.self) else {
            return defaultObject
        }
        
        return object
    }
}

