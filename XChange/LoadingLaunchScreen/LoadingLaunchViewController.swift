//
//  LoadingLaunchViewController.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import UIKit
import Swinject
import SnapKit
import RxSwift

class LoadingLaunchViewController: UIViewController, ViewControllerContract {

    var container: Container?
    var currencyExchangeViewModel: CurrencyExchangeViewModel?
    
    var disposeBag = DisposeBag()
    let dispatchGroup = DispatchGroup()
    
    let logoView = UIView()
    let logoIcon = UIImageView()
    let logoTextImageView = UIImageView()
    
    var logoViewWidthConstraint: Constraint?
    
    var currencies: [CurrencyViewModel]?
    
    enum Config {
        static let mainBackgroundColor      = UIColor.white
        
        static let logoIcon                 = UIImage(named: "x_icon")
        static let logoTextImage            = UIImage(named: "change")
        
        static let logoHeightMultiplier     = CGFloat(0.1207)
        static let logoWidthRatio           = CGFloat(5.36612022)
        static let logoIconWidthRatio       = CGFloat(0.75409836)
        static let logoTextImageWidthRatio  = CGFloat(4.61202186)
        
        static let animationDuration        = TimeInterval(0.5)
    }
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupSubscribers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateInfo()
    }
    
    // MARK: Setup
    
    func setupSubviews() {
        view.backgroundColor = Config.mainBackgroundColor
        
        setupLogoView()
    }
    
    func setupLogoView() {
        setupLogoTextImageView()
        setupLogoIcon()
        
        logoView.clipsToBounds = true
        view.addSubview(logoView)
        
        logoView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.height.equalTo(view.snp.width).multipliedBy(Config.logoHeightMultiplier)
            make.centerY.equalToSuperview()
            logoViewWidthConstraint = make.width.equalTo(logoIcon.snp.height).multipliedBy(Config.logoIconWidthRatio).constraint
        }
    }
    
    func setupLogoTextImageView() {
        logoTextImageView.image = Config.logoTextImage
        logoTextImageView.contentMode = .scaleAspectFill
        logoView.addSubview(logoTextImageView)
        
        logoTextImageView.snp.makeConstraints { (make) in
            make.top.bottom.right.equalToSuperview()
            make.width.equalTo(logoTextImageView.snp.height).multipliedBy(Config.logoTextImageWidthRatio)
        }
    }
    
    func setupLogoIcon() {
        logoIcon.image = Config.logoIcon
        logoIcon.backgroundColor = Config.mainBackgroundColor
        logoIcon.contentMode = .scaleAspectFit
        logoIcon.clipsToBounds = true
        logoView.addSubview(logoIcon)
        
        logoIcon.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview()
            make.width.equalTo(logoIcon.snp.height).multipliedBy(Config.logoIconWidthRatio)
        }
    }
    
    func setupSubscribers() {
        currencyExchangeViewModel?.baseCurrencyCode = "USD"
        
        currencyExchangeViewModel?.exchangeRatesSubject.subscribe(onNext: { [weak self] (exchangeRates) in
            if let _ = exchangeRates {
                self?.dispatchGroup.leave()
            }
        }).disposed(by: disposeBag)
        
        currencyExchangeViewModel?.currenciesSubject.subscribe(onNext: { [weak self] (currencies) in
            if let _ = currencies {
                self?.currencies = currencies
                self?.dispatchGroup.leave()
            }
        }).disposed(by: disposeBag)
    }
    
    // MARK: Update info
    
    func updateInfo() {
        dispatchGroup.enter()
        currencyExchangeViewModel?.forceExchangeRatesUpdate()
        
        dispatchGroup.enter()
        currencyExchangeViewModel?.loadCurrenciesInfo()
        
        dispatchGroup.notify(queue: DispatchQueue.main) { [weak self] in
            self?.didCompleteRatesLoading()
        }
    }
    
    // MARK: Animations
    
    func didCompleteRatesLoading() {
        logoViewWidthConstraint?.deactivate()
        
        logoView.snp.makeConstraints { (make) in
            logoViewWidthConstraint = make.width.equalTo(logoIcon.snp.height).multipliedBy(Config.logoWidthRatio).constraint
        }
        
        let animations = {
            self.view.layoutIfNeeded()
        }
        
        let completion = { (completed: Bool) -> Void in
            guard let currencyConverterVC = self.container?.resolve(CurrencyConverterViewController.self) else {
                return
            }
            
            if let currencies = self.currencies {
                currencyConverterVC.currencies = currencies
            }
            
            self.disposeBag = DisposeBag()
            
            currencyConverterVC.currencyExchangeViewModel = self.currencyExchangeViewModel
            currencyConverterVC.baseCurrency = self.currencyExchangeViewModel?.baseCurrency
            
            self.present(currencyConverterVC, animated: false, completion: nil)
        }
        
        UIView.animate(withDuration: Config.animationDuration,
                       animations: animations,
                       completion: completion)
    }
}
