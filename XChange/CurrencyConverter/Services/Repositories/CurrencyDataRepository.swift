//
//  CurrencyDataRepository.swift
//  XChange
//
//  Created by Diego Rincon on 11/6/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

class CurrencyDataRepository: CurrencyDataRepositoryProtocol {
    enum Config {
        static let fileName         = "currencies"
        static let fileExtension    = "json"
    }
    
    func loadCurrencyData(completion: (Result<[CurrencyDataJSON], ReadFileError>) -> Void) {
        guard let fileURL = Bundle.main.url(forResource: Config.fileName, withExtension: Config.fileExtension) else {
            completion(.failure(.fileNotFound))
            return
        }
        
        guard let fileData = try? Data(contentsOf: fileURL) else {
            completion(.failure(.unableToOpenFile))
            return
        }
        
        guard
            let jsonData = try? JSONSerialization.jsonObject(with: fileData, options: .allowFragments),
            let currenciesJSON = jsonData as? [CurrencyDataJSON] else {
                completion(.failure(.invalidJSON))
                return
        }
        
        completion(.success(currenciesJSON))
    }
}
