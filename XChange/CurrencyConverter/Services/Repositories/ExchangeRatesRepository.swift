//
//  ExchangeRatesRepository.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation
import Alamofire

class ExchangeRatesRepository: ExchangeRatesRepositoryProtocol {
    
    enum Config {
        static let baseKey  = "base"
    }
    
    var appConfig: AppConfig?
    
    func requestExchangeRates(baseCurrencyCode: String, completion: @escaping RequestExchangeRatesCompletionClosure) {
        guard
            let scheme = appConfig?.scheme,
            let host = appConfig?.host,
            let endpoint = appConfig?.rateExchangeEndpoint else {
                completion(.failure(.unableToMakeRequest))
                return
        }
        
        let baseItem = URLQueryItem(name: Config.baseKey, value: baseCurrencyCode)
        
        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = host
        urlComponents.path = endpoint
        urlComponents.queryItems = [baseItem]
        
        guard let url = urlComponents.url else {
            completion(.failure(.unableToMakeRequest))
            return
        }
        
        let request = Alamofire.request(url,
                                        method: .get,
                                        parameters: nil,
                                        encoding: JSONEncoding.prettyPrinted,
                                        headers: nil)
        
        request.response { (response) in
            if let _ = response.error {
                completion(.failure(.requestFailed))
                return
            }
            
            guard let data = response.data else {
                completion(.failure(.emptyResponse))
                return
            }
            
            guard
                let jsonObject = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                let exchangeRatesJSON = jsonObject as? [String: Any],
                let exchangeRates = ExchangeRatesModel(json: exchangeRatesJSON) else {
                    completion(.failure(RequestError.invalidJSON))
                    return
            }
            
            completion(.success(exchangeRates))
        }
    }
    
    func save(exchangeRates: ExchangeRatesModel, fileName: String) -> Bool {
        return false
    }
    
    func loadExchangeRates(fileName: String) -> ExchangeRatesModel? {
        return nil
    }
}
