//
//  FauxExchangeRatesRepository.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

class FauxExchangeRatesRepository: ExchangeRatesRepositoryProtocol {
    
    var responseTime = Double(0.5)
    var requestError: RequestError?
    
    func requestExchangeRates(baseCurrencyCode: String, completion: @escaping RequestExchangeRatesCompletionClosure) {
        if let requestError = requestError {
            Delay.main(seconds: responseTime, execute: {
                completion(.failure(requestError))
            })
            
            return
        }
        
        guard
            let exchangeRatesJSON = TestHelper.jsonDicFromJSONFile(named: "ExchangeRatesCorrect"),
            let exchangeRatesModel = ExchangeRatesModel(json: exchangeRatesJSON) else {
                completion(.failure(.invalidJSON))
                return
        }
        
        Delay.main(seconds: responseTime) {
            completion(.success(exchangeRatesModel))
        }
    }
    
    func save(exchangeRates: ExchangeRatesModel, fileName: String) -> Bool {
        return false
    }
    
    func loadExchangeRates(fileName: String) -> ExchangeRatesModel? {
        return nil
    }
}
