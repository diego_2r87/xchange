//
//  ExchangeRatesRepositoryProtocol.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

enum RequestError: Error {
    case unableToMakeRequest
    case requestFailed
    case emptyResponse
    case invalidJSON
    case authenticationFailed
}

typealias RequestExchangeRatesCompletionClosure = (_ result: Result<ExchangeRatesModel, RequestError>) -> Void

protocol ExchangeRatesRepositoryProtocol {
    func requestExchangeRates(baseCurrencyCode: String, completion: @escaping RequestExchangeRatesCompletionClosure)
    func save(exchangeRates: ExchangeRatesModel, fileName: String) -> Bool
    func loadExchangeRates(fileName: String) -> ExchangeRatesModel?
}
