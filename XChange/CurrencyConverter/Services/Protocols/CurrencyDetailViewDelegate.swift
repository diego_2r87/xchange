//
//  CurrencyDetailViewDelegate.swift
//  XChange
//
//  Created by Diego Rincon on 11/6/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

protocol CurrencyDetailViewDelegate: class {
    func baseCurrencyDidChange(currencyDetailView: CurrencyDetailView, value: Double)
}
