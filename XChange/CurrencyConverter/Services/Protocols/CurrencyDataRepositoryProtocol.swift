//
//  CurrencyDataRepositoryProtocol.swift
//  XChange
//
//  Created by Diego Rincon on 11/6/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

enum ReadFileError: Error {
    case fileNotFound
    case unableToOpenFile
    case invalidJSON
}

typealias CurrencyDataJSON = [String: String]
typealias CurrencyDataCompletionClosure = (_ result: Result<[CurrencyDataJSON], ReadFileError>) -> Void

protocol CurrencyDataRepositoryProtocol {
    func loadCurrencyData(completion: CurrencyDataCompletionClosure)
}
