//
//  ExchangeRatesModel.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation

struct ExchangeRatesModel: Codable {
    
    enum Config {
        static let baseKey      = "base"
        static let dateKey      = "date"
        static let ratesKey     = "rates"
    }
    
    let baseCurrencyCode: String
    let update: String?
    let exchangeRates: [String: Double]
    
    init?(json: [String: Any]) {
        let parser = JSONParser(json: json)
        
        guard
            let code = parser.objectForKey(Config.baseKey, classType: String.self),
            let rates = parser.objectForKey(Config.ratesKey, classType: [String: Any].self) else {
                return nil
        }
        
        baseCurrencyCode = code
        update = parser.objectForKey(Config.dateKey, classType: String.self)
        
        var validRates = [String: Double]()
        
        for (key, value) in rates {
            if let exchangeRate = value as? Double {
                validRates[key] = exchangeRate
            }
        }
        
        exchangeRates = validRates
    }
}
