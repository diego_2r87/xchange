//
//  CurrencyViewModel.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import UIKit

struct CurrencyViewModel {
    enum Config {
        static let flagKey      = "flag"
        static let codeKey      = "code"
        static let nameKey      = "name"
        static let symbolKey    = "symbol"
    }
    
    let flag: UIImage
    let code: String
    let name: String
    let symbol: String
    
    init?(json: [String: String]) {
        guard
            let flagName = json[Config.flagKey],
            let flag = UIImage(named: flagName),
            let code = json[Config.codeKey],
            let name = json[Config.nameKey],
            let symbol = json[Config.symbolKey] else {
                return nil
        }

        self.flag = flag
        self.code = code
        self.name = name
        self.symbol = symbol
    }
}
