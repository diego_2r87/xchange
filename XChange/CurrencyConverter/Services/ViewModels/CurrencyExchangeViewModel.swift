//
//  CurrencyExchangeViewModel.swift
//  XChange
//
//  Created by Diego Rincon on 11/2/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import Foundation
import RxSwift

class CurrencyExchangeViewModel {
    
    enum Config {
        static let defaultCurrencyCode      = "USD"
    }
    
    var exchangeRatesRepository: ExchangeRatesRepositoryProtocol?
    var currencyDataRepository: CurrencyDataRepository?
    
    let exchangeRatesSubject = BehaviorSubject<[String: Double]?>(value: nil)
    let currenciesSubject = BehaviorSubject<[CurrencyViewModel]?>(value: nil)
    
    var baseCurrencyCode: String?
    
    var baseCurrency: CurrencyViewModel?
    
    func forceExchangeRatesUpdate() {
        exchangeRatesRepository?.requestExchangeRates(baseCurrencyCode: Config.defaultCurrencyCode, completion: { [weak self] (result) in
            switch result {
            case .failure(let error):
                self?.exchangeRatesSubject.onError(error)
            case .success(let exchangeRatesModel):
                self?.exchangeRatesSubject.onNext(exchangeRatesModel.exchangeRates)
            }
        })
    }
    
    func exchangeUSD(total: Double, to code: String) -> Double {
        guard
            let exchangeRates = try? exchangeRatesSubject.value(),
            let exchangeRate = exchangeRates?[code] else {
                return 0.0
        }
        
        return total * exchangeRate
    }
    
    func loadCurrenciesInfo() {
        currencyDataRepository?.loadCurrencyData(completion: { (result) in
            switch result {
            case .success(let currenciesData):
                var currencies = [CurrencyViewModel]()
                
                for currencyData in currenciesData {
                    guard let currency = CurrencyViewModel(json: currencyData) else {
                        continue
                    }
                    
                    if currency.code == self.baseCurrencyCode {
                        self.baseCurrency = currency
                    }
                    else {
                        currencies.append(currency)
                    }
                }
                
                self.currenciesSubject.onNext(currencies)
            case .failure(let error):
                self.currenciesSubject.onError(error)
            }
        })
    }
}
