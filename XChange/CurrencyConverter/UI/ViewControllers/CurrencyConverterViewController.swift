//
//  CurrencyConverterViewController.swift
//  XChange
//
//  Created by Diego Rincon on 10/31/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import UIKit
import Swinject
import SnapKit
import RxSwift

enum UIState {
    case initial, transitioning, ready
}

class CurrencyConverterViewController: UIViewController, ViewControllerContract {

    enum Config {
        static let currencyDetailHeight     = CGFloat(80.0)
        static let topMargin                = CGFloat(28.0)
        static let titleHeight              = CGFloat(20.0)
        
        static let logoWidthRatio           = CGFloat(5.36612022)
        static let logoHeightMultiplier     = CGFloat(0.1207)
        
        static let animationDuration        = TimeInterval(0.5)
        
        static let titleImage               = UIImage(named: "xchange")
        
        static let mainBackgroundColor      = UIColor.white
        static let listBackgroundColor      = UIColor(red255: 55, green255: 56, blue255: 58)
        static let refreshControlColor      = UIColor(red255: 166, green255: 166, blue255: 168)
    }
    
    var container: Container?
    var currencyExchangeViewModel: CurrencyExchangeViewModel?
    
    let tableView = UITableView()
    let refreshControl = UIRefreshControl()
    let baseCurrencyView = CurrencyDetailView()
    let titleImageView = UIImageView()
    
    let disposeBag = DisposeBag()
    
    var titleVPosConstraint: Constraint?
    var titleHeightConstraint: Constraint?
    
    var currencies = [CurrencyViewModel]()
    var baseCurrency: CurrencyViewModel?
    var baseValue = Double(0.0)
    
    var state: UIState = .initial {
        didSet {
            updateSubviews(with: state)
        }
    }
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupSubscribers()
        state = .initial
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        animateTransition(to: .transitioning) { (_) in
            self.setupTableView()
            self.animateTransition(to: .ready, completion: nil)
        }
    }
    
    // MARK: Setup
    
    func setupSubviews() {
        view.backgroundColor = Config.mainBackgroundColor
        
        setupTitleImageView()
        setupBaseCurrencyView()
    }
    
    func setupTitleImageView() {
        titleImageView.image = Config.titleImage
        titleImageView.contentMode = .scaleAspectFit
        titleImageView.clipsToBounds = true
        view.addSubview(titleImageView)
        
        titleImageView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.width.equalTo(titleImageView.snp.height).multipliedBy(Config.logoWidthRatio)
        }
    }
    
    func setupBaseCurrencyView() {
        if let baseCurrency = baseCurrency {
            baseCurrencyView.load(currency: baseCurrency)
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOnBaseCurrencyView(_:)))
        
        baseCurrencyView.addGestureRecognizer(tapGesture)
        baseCurrencyView.delegate = self
        baseCurrencyView.alpha = 0.0
        view.addSubview(baseCurrencyView)
        
        baseCurrencyView.snp.makeConstraints { (make) in
            make.top.equalTo(titleImageView.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(Config.currencyDetailHeight)
        }
    }
    
    func setupTableView() {
        refreshControl.tintColor = Config.refreshControlColor
        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
        
        tableView.alpha = 0.0
        tableView.backgroundColor = Config.listBackgroundColor
        tableView.register(CurrencyDetailCell.self, forCellReuseIdentifier: CurrencyDetailCell.reuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.addSubview(refreshControl)
        tableView.keyboardDismissMode = .onDrag
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(baseCurrencyView.snp.bottom)
            make.left.bottom.right.equalToSuperview()
        }
    }
    
    func setupSubscribers() {
        currencyExchangeViewModel?.exchangeRatesSubject.subscribe(onNext: { [weak self] (exchangeRates) in
            if let _ = exchangeRates {
                self?.tableView.reloadData()
                self?.refreshControl.endRefreshing()
            }
        }).disposed(by: disposeBag)
    }
    
    // MARK: Action methods
    
    @objc func didPullToRefresh(_ sender: UIRefreshControl) {
        currencyExchangeViewModel?.forceExchangeRatesUpdate()
    }
    
    // MARK: UI State updates
    
    func updateSubviews(with state: UIState) {
        updateTitle(with: state)
        updateBaseCurrencyView(with: state)
        updateTableView(with: state)
    }
    
    func updateTitle(with state: UIState) {
        titleHeightConstraint?.deactivate()
        titleVPosConstraint?.deactivate()
        
        switch state {
        case .initial:
            titleImageView.snp.makeConstraints { (make) in
                titleHeightConstraint = make.height.equalTo(view.snp.width).multipliedBy(Config.logoHeightMultiplier).constraint
                titleVPosConstraint = make.centerY.equalToSuperview().constraint
            }
        case .transitioning, .ready:
            titleImageView.snp.makeConstraints({ (make) in
                titleHeightConstraint = make.height.equalTo(Config.titleHeight).constraint
                titleVPosConstraint = make.top.equalToSuperview().offset(Config.topMargin).constraint
            })
        }
    }
    
    func updateBaseCurrencyView(with state: UIState) {
        switch state {
        case .initial, .transitioning:
            baseCurrencyView.alpha = 0.0
        case .ready:
            baseCurrencyView.alpha = 1.0
        }
    }
    
    func updateTableView(with state: UIState) {
        switch state {
        case .initial, .transitioning:
            tableView.alpha = 0.0
        case .ready:
            tableView.alpha = 1.0
        }
    }
    
    // MARK: Auxiliary methods
    
    func animateTransition(to state: UIState, duration: TimeInterval = Config.animationDuration, completion: ((_ completed: Bool) -> Void)?) {
        let animations = {
            self.state = state
            self.view.layoutIfNeeded()
        }
        
        view.layoutIfNeeded()
        UIView.animate(withDuration: duration,
                       animations: animations,
                       completion: completion)
    }
    
    // MARK: Action methods
    
    @objc func didTapOnBaseCurrencyView(_ gestureRecognizer: UITapGestureRecognizer) {
        baseCurrencyView.startEditing()
    }
}

extension CurrencyConverterViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let currencyDetailCell = tableView.dequeueReusableCell(withIdentifier: CurrencyDetailCell.reuseIdentifier, for: indexPath) as? CurrencyDetailCell,
            let currency = currencies[safe: indexPath.row],
            let currencyValue = currencyExchangeViewModel?.exchangeUSD(total: baseValue, to: currency.code) else {
                return UITableViewCell()
        }
        
        currencyDetailCell.selectionStyle = .none
        currencyDetailCell.backgroundColor = Config.listBackgroundColor
        currencyDetailCell.load(currency: currency)
        currencyDetailCell.load(value: currencyValue)

        return currencyDetailCell
    }
}

extension CurrencyConverterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Config.currencyDetailHeight
    }
}

extension CurrencyConverterViewController: CurrencyDetailViewDelegate {
    func baseCurrencyDidChange(currencyDetailView: CurrencyDetailView, value: Double) {
        baseValue = value
        tableView.reloadData()
    }
}
