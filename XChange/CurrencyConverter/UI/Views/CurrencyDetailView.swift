//
//  CurrencyDetailView.swift
//  XChange
//
//  Created by Diego Rincon on 10/31/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import UIKit
import SnapKit

class CurrencyDetailView: UIView {
    
    enum Config {
        static let verticalMarginMultiplier     = CGFloat(0.2)
        static let horizontalMarginMultiplier   = CGFloat(0.03)
        
        static let codeHeight                   = CGFloat(25.0)
        static let codeWidth                    = CGFloat(50.0)
        static let nameHeight                   = CGFloat(12.0)
        static let totalDefaultWidth            = CGFloat(10.0)
        static let symbolWidth                  = CGFloat(40.0)
        static let horizontalSpacing            = CGFloat(12.0)
        static let verticalSpacing              = CGFloat(8.0)
        
        static let codeFont                     = UIFont(name: "HelveticaNeue", size: 20.0)
        static let totalFont                    = UIFont(name: "HelveticaNeue", size: 17.0)
        static let nameFont                     = UIFont(name: "HelveticaNeue", size: 12.0)
        
        static let codeColor                    = UIColor(red255: 120, green255: 120, blue255: 122)
        static let totalColor                   = UIColor(red255: 166, green255: 166, blue255: 168)
        static let nameColor                    = UIColor(red255: 88, green255: 88, blue255: 89)
    }
    
    var isEditable = true {
        didSet {
            totalTextField.isUserInteractionEnabled = isEditable
        }
    }
    
    weak var delegate: CurrencyDetailViewDelegate?
    
    // MARK: Private properties
    
    private let flagView = UIImageView()
    private let codeLabel = UILabel()
    private let nameLabel = UILabel()
    private let symbolLabel = UILabel()
    private let totalTextField = UITextField()
    
    private var totalWidthConstraint: Constraint?
    
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }

    // MARK: Initializers
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MAKR: Setup
    
    private func setupSubviews() {
        setupFlagView()
        setupCodeLabel()
        setupNameLabel()
        setupTotalTextField()
        setupSymbolLabel()
    }
    
    private func setupFlagView() {
        flagView.contentMode = .scaleAspectFit
        flagView.clipsToBounds = true
        addSubview(flagView)
        
        flagView.snp.makeConstraints { (make) in
            make.top.equalTo(snp.bottom).multipliedBy(Config.verticalMarginMultiplier)
            make.left.equalTo(snp.right).multipliedBy(Config.horizontalMarginMultiplier)
            make.bottom.equalToSuperview().multipliedBy(1 - Config.verticalMarginMultiplier)
            make.width.equalTo(flagView.snp.height)
        }
    }
    
    private func setupCodeLabel() {
        codeLabel.font = Config.codeFont
        codeLabel.textColor = Config.codeColor
        addSubview(codeLabel)
        
        codeLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(flagView)
            make.left.equalTo(flagView.snp.right).offset(Config.horizontalSpacing)
            make.height.equalTo(Config.codeHeight)
            make.width.equalTo(Config.codeWidth)
        }
    }
    
    private func setupNameLabel() {
        nameLabel.font = Config.nameFont
        nameLabel.textColor = Config.nameColor
        nameLabel.textAlignment = .right
        addSubview(nameLabel)
        
        nameLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(flagView)
            make.right.equalToSuperview().multipliedBy(1 - Config.horizontalMarginMultiplier)
            make.height.equalTo(Config.nameHeight)
            make.left.equalTo(codeLabel.snp.right).offset(Config.horizontalSpacing)
        }
    }
    
    private func setupTotalTextField() {
        totalTextField.text = "0"
        totalTextField.font = Config.totalFont
        totalTextField.textColor = Config.totalColor
        totalTextField.textAlignment = .right
        totalTextField.keyboardType = .decimalPad
        totalTextField.isUserInteractionEnabled = isEditable
        totalTextField.keyboardAppearance = .dark
        totalTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        addSubview(totalTextField)
        
        totalTextField.snp.makeConstraints { (make) in
            let leftOffset = Config.horizontalSpacing + Config.symbolWidth
            
            make.right.equalTo(nameLabel)
            make.top.equalTo(flagView)
            make.bottom.equalTo(nameLabel.snp.top).offset(-Config.verticalSpacing)
            make.left.greaterThanOrEqualTo(codeLabel.snp.right).offset(leftOffset)
            totalWidthConstraint = make.width.equalTo(Config.totalDefaultWidth).priority(.medium).constraint
        }
    }
    
    private func setupSymbolLabel() {
        symbolLabel.font = Config.totalFont
        symbolLabel.textColor = Config.totalColor
        symbolLabel.textAlignment = .right
        addSubview(symbolLabel)
        
        symbolLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(totalTextField)
            make.right.equalTo(totalTextField.snp.left)
            make.width.equalTo(Config.symbolWidth)
        }
    }
    
    // MARK: Auxiliary methods
    
    func load(currency: CurrencyViewModel) {
        flagView.image = currency.flag
        codeLabel.text = currency.code
        nameLabel.text = currency.name
        symbolLabel.text = currency.symbol
    }
    
    func load(value: Double) {
        totalTextField.text = String(format: "%.2f", value)
    }
    
    func startEditing() {
        if isEditable {
            totalTextField.becomeFirstResponder()
        }
    }
    
    // MARK: Action methods
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else {
            return
        }
        
        let valueString: String
        
        if text == "" {
            valueString = "0"
        }
        else if text.hasPrefix("0.") || text.hasPrefix("0") == false || text == "0" {
            valueString = text
        }
        else {
            valueString = String(text.dropFirst())
        }
        
        textField.text = valueString
        
        if let value = Double(valueString) {
            delegate?.baseCurrencyDidChange(currencyDetailView: self, value: value)
        }
    }
}
