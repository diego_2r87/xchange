//
//  CurrencyDetailCell.swift
//  XChange
//
//  Created by Diego Rincon on 10/31/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import UIKit

class CurrencyDetailCell: UITableViewCell {

    static let reuseIdentifier = "CurrencyDetailCell"
    
    private let currencyDetailView = CurrencyDetailView()
    
    // MARK: Initializers
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupCurrencyDetailView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Setup
    
    private func setupCurrencyDetailView() {
        currencyDetailView.isEditable = false
        contentView.addSubview(currencyDetailView)
        
        currencyDetailView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalToSuperview()
        }
    }
    
    // MARK: Auxiliary methods
    
    func load(currency: CurrencyViewModel) {
        currencyDetailView.load(currency: currency)
    }
    
    func load(value: Double) {
        currencyDetailView.load(value: value)
    }
}
