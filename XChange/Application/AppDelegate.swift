//
//  AppDelegate.swift
//  XChange
//
//  Created by Diego Rincon on 10/31/17.
//  Copyright © 2017 Scire Studios. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var swinjectManager: SwinjectManager?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupServices()
        setupRootViewController()
        
        return true
    }
    
    // MARK: Setup
    
    func setupServices() {
        swinjectManager = SwinjectManager()
    }
    
    func setupRootViewController() {
        let loadingLaunchVC = swinjectManager?.container.resolve(LoadingLaunchViewController.self)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = loadingLaunchVC
        window?.makeKeyAndVisible()
    }
}

